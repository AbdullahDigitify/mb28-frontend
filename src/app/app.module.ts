import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HeroComponentsComponent } from './hero-components/hero-components.component';
import { PageNotFoundComponentComponent } from './page-not-found-component/page-not-found-component.component';

const appRoutes: Routes = [
  { path: 'hero-component', component: HeroComponentsComponent },
  { path: '**', component: PageNotFoundComponentComponent },

];

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HeroComponentsComponent,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  declarations: [
    AppComponent,
    HeroComponentsComponent,
    PageNotFoundComponentComponent,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
